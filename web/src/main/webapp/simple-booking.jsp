<%@ page import="services.beans.HotelBookingManager" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Booking</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<div>
<%
	final Hashtable jndiProperties = new Hashtable();
    jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
    final Context context = new InitialContext(jndiProperties);
    
    // Inject hotel booking manager bean
    HotelBookingManager bm = (HotelBookingManager) context.lookup("ejb:/service/HotelBookingManagerBean!services.beans.HotelBookingManager");
    
    String formAction = request.getParameter("formAction");
    
    if(formAction != null)
    {
    	if(formAction.equals("AddBooking")) 
    	{    
    		// Get form field values from request params
    	    String firstName = request.getParameter("firstName").trim();
    	    String lastName = request.getParameter("lastName").trim();
    	    String dateOfBirth = request.getParameter("dateOfBirth").trim();
    	    String roomType = request.getParameter("roomType").trim();
    	    String checkInDate = request.getParameter("checkInDate").trim();
    	    String checkOutDate = request.getParameter("checkOutDate").trim();

    	    Long newBooking = bm.book(firstName, lastName, dateOfBirth, roomType, checkInDate, checkOutDate);
    	    
    	    if(newBooking == null) 
    	    	response.sendRedirect("simple-error.jsp?error_type=booking");
    	    else
    	    {
    	    	%><strong>Booking successful! Reference number:</strong> <%= newBooking %><br/><%
			}
    	}
    	else if(formAction.equals("CancelBooking"))
   	    {	
    		if(request.getParameter("referenceNumber") != null && !request.getParameter("referenceNumber").equals(""))
    		{
    			try
    			{
    				Long referenceNumber = Long.parseLong(request.getParameter("referenceNumber"));
        			boolean cancelled = bm.cancel(referenceNumber);
           	    	
           	    	if(!cancelled)
           	    		response.sendRedirect("simple-error.jsp?error_type=cancellation");
           	    	else
           	    	{ 
           	    		%><strong>Booking succesfully canceled!</strong><br/><%
           	    	}
    			}
    			catch(NumberFormatException e)
    			{
    				response.sendRedirect("simple-error.jsp?error_type=cancellation");
    			}
    		}
    		else
    		{
    			response.sendRedirect("simple-error.jsp?error_type=cancellation");
    		}
   	    }
    }
    else 
    {
   		response.sendRedirect("simple-error.jsp");
    }
%>
<a href="simple.jsp" title="Book a hotel">Back to booking page</a>
</div>
</body>
</html>