(function(window, $) {
$(function() {
	
	$('.datepicker-v1').datepicker({
		dateFormat: 'dd-mm-yy',
		changeYear: true,
		changeMonth: true,
		yearRange: '1920:2020'
	});
	
	$('.datepicker-v2').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	
	$('.datepicker-v3').datepicker({
		dateFormat: 'dd/mm/yy'
	});
	
});
})(window, jQuery);