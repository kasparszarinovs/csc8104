<%@ page import="services.beans.HotelBookingManager" %>
<%@ page import="services.beans.BookingManager" %>
<%@ page import="services.beans.TaxiService" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Travel Agent Booking</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<div>
<%
	final Hashtable jndiProperties = new Hashtable();
	jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	final Context context = new InitialContext(jndiProperties);
	
	// Inject all booking manager beans
	HotelBookingManager hbm = (HotelBookingManager) context.lookup("ejb:/service/HotelBookingManagerBean!services.beans.HotelBookingManager");
	BookingManager fbm = (BookingManager) context.lookup("ejb:/service/BookingManagerBean!services.beans.BookingManager");
	TaxiService tbm = (TaxiService) context.lookup("ejb:/service/TaxiServiceBean!services.beans.TaxiService");
	
	// Get form action from request params
	String formAction = request.getParameter("formAction");
	
	// Set initial values for booking status flags
	boolean flightOK = false;
	Long flightCancelled = null;
	boolean taxiOK = false;
	boolean taxiCancelled = false;
	boolean hotelOK = false;
	
	// Define bookings and assign initial values
	Long newFlightBooking = null;
	String newTaxiBooking = null;
	Long newHotelBooking = null;
	
	if(formAction != null && formAction.equals("TravelAgentBooking"))
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		// Get form field values
		String firstName = request.getParameter("firstName").trim();
		String lastName = request.getParameter("lastName").trim();
		String dateOfBirth = request.getParameter("dateOfBirth").trim();
		String flightName = request.getParameter("flightName").trim();
		String departureDate = request.getParameter("departureDate").trim();
		int sizeOfParty = Integer.parseInt(request.getParameter("sizeOfParty").trim());
		Date taxiDate = sdf.parse(request.getParameter("taxiDate").trim());
		String roomType = request.getParameter("roomType").trim();
		String checkInDate = request.getParameter("checkInDate").trim();
		String checkOutDate = request.getParameter("checkOutDate").trim();
		
		newFlightBooking = fbm.makeBooking(firstName + " " + lastName, flightName, departureDate);
		
		if(newFlightBooking != null)
		{
			/* Flight booking successful */
			flightOK = true;
			
			newTaxiBooking = tbm.book(firstName, lastName, sizeOfParty, taxiDate);
			if(newTaxiBooking != null)
			{
				/* Taxi booking successful */
				taxiOK = true;
				newHotelBooking = hbm.book(firstName, lastName, dateOfBirth, roomType, checkInDate, checkOutDate);
				
				if(newHotelBooking != null)
				{ 
					/* Hotel booking successful */
					hotelOK = true;
				}
				else
				{
					/* Hotel booking failed */
					flightCancelled = fbm.cancelBooking(newFlightBooking);
					taxiCancelled = tbm.cancel(newTaxiBooking);
				}
			}
			else
			{
				/* Taxi booking failed */
				flightCancelled = fbm.cancelBooking(newFlightBooking);
			}
		}
		/* else Flight booking failed */
	}
	else
	{
		response.sendRedirect("travel-agent-error.jsp");
	}

// Generate output based on status flags
%><p>Flight booking: <%
if(flightOK) 
{ 
	%><strong>Successful</strong><%
	if(!taxiOK || !hotelOK)
	{
		if(flightCancelled != null) { %> [Sucessfully cancelled]<% }
		else { %> [Failed to cancel]<% }
	}
}
else { %><em>Failed</em> <% } 
%></p>

<p>Taxi booking: <%
if(taxiOK)
{
	%><strong>Successful</strong><%
	if(!hotelOK)
	{
		if(taxiCancelled) { %> [Sucessfully cancelled]<% }
		else { %> [Failed to cancel]<% }
	}
} 
else { %><em>Failed </em><% }
%></p>

<p>Hotel booking: <%
if(hotelOK) 
{ 
	%><strong>Successful</strong><% 
} 
else { %><em>Failed</em><% } 
%></p>

<a href="travel-agent.jsp" title="Travel Agent">Back to Travel Agent</a>

</div>
</body>
</html>