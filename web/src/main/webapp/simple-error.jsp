<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Booking Error</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<div>
<%
    String errorType = request.getParameter("error_type");
    
    if(errorType != null && (errorType.equals("booking") || errorType.equals("cancellation")))
    {
	    %><strong><%=errorType %> unsuccessful!</strong><br/><% 
    }
    else
    { 
    	%><strong>Something has gone terribly wrong...</strong><br/><% 
    }
%>
<a href="simple.jsp" title="Book a hotel">Back to booking page</a>
</div>
</body>
</html>