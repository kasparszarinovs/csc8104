<%@ page import="services.beans.HotelBookingManager" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Simple Booking</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.min.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<%
	final Hashtable jndiProperties = new Hashtable();
    jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
    final Context context = new InitialContext(jndiProperties);

    HotelBookingManager bm = (HotelBookingManager) context.lookup("ejb:/service/HotelBookingManagerBean!services.beans.HotelBookingManager");
    List<String> rooms = bm.getAllRoomTypes();
%>

<form action="simple-booking.jsp" method="post">
<div class="book">

	<h1>Book a hotel</h1>
	
	<div class="details">
		<div class="customer-details">
			<h2>Customer details</h2>
			<fieldset>
				<label for="firstName">First name:</label>
				<input type="text" name="firstName" id="firstName" />
			</fieldset>
			<fieldset>
				<label for="lastName">Last name:</label>
				<input type="text" name="lastName" id="lastName" />
			</fieldset>
			<fieldset>
				<label for="dateOfBirth">Date of birth:</label>
				<input type="date" name="dateOfBirth"  id="dateOfBirth" class="datepicker-v1" autocomplete="off" />
			</fieldset>
		</div>
		
		<div class="room-details">
			<h2>Room details</h2>
			<fieldset>
				<label for="roomType">Room type: </label>
				<select name="roomType" id="roomType">
					<option value="">-- Select type --</option>
					<%
						for(String r : rooms) {
							out.println("<option value=\"" + r + "\">" + r.substring(0,1).toUpperCase() + r.substring(1).toLowerCase() + "</option>");
						}
					%>
				</select>
			</fieldset>
			<fieldset>
				<label for="checkInDate">Check in date:</label>
				<input type="date" name="checkInDate" id="checkInDate" class="datepicker-v2" autocomplete="off" />
			</fieldset>
			<fieldset>
				<label for="checkOutDate">Check out date:</label>
				<input type="date" name="checkOutDate" id="checkOutDate" class="datepicker-v2" autocomplete="off" />
			</fieldset>
			<fieldset>
				<input type="hidden" name="formAction" value="AddBooking" />
				<input type="submit" name="submitBooking" id="submitBooking" />
			</fieldset>
		</div>
	</div>
	
</div>
</form>

<form action="simple-booking.jsp" method="post">
<div class="cancel">

	<h1>Cancel booking</h1>

	<fieldset>
		<label for="referenceNumber">Booking reference number: </label>
		<input type="text" name="referenceNumber" id="referenceNumber" />
	</fieldset>
	<fieldset>	
		<input type="hidden" name="formAction" value="CancelBooking" />
		<input type="submit" name="submitCancellation" id="submitCancellation" />
	</fieldset>
	
</div>
</form>


<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
