<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Travel Agent Error</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>

<div>
	<strong>Error: no bookings initiated</strong><br/>
	<a href="travel-agent.jsp" title="Travel Agent">Back to Travel Agent</a>
</div>

</body>
</html>