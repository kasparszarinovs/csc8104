<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Transactions Error</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>

<div>
	<strong>Error: no transaction bookings initiated</strong><br/>
	<a href="transactions.jsp" title="Transactions">Back to Transactions</a>
</div>

</body>
</html>