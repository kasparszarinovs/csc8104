<%@ page import="services.beans.HotelBookingManager" %>
<%@ page import="services.beans.BookingManager" %>
<%@ page import="services.beans.TaxiService" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="javax.transaction.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Transactions Booking</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<div>
<%
	final Hashtable jndiProperties = new Hashtable();
	jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	final Context context = new InitialContext(jndiProperties);
	
	// Define user transaction
	UserTransaction utx = (UserTransaction) context.lookup("java:comp/UserTransaction");
	
	// Inject all manager beans
	HotelBookingManager hbm = (HotelBookingManager) context.lookup("ejb:/service/HotelBookingManagerBean!services.beans.HotelBookingManager");
	BookingManager fbm = (BookingManager) context.lookup("ejb:/service/BookingManagerBean!services.beans.BookingManager");
	TaxiService tbm = (TaxiService) context.lookup("ejb:/service/TaxiServiceBean!services.beans.TaxiService");
	
	// Get form action from request parameters
	String formAction = request.getParameter("formAction");
	
	// Define bookings and assign initial values
	Long newFlightBooking = null;
	String newTaxiBooking = null;
	Long newHotelBooking = null;
	
	if(formAction != null && formAction.equals("TransactionsBooking"))
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		// Get all posted form field values
		String firstName = request.getParameter("firstName").trim();
		String lastName = request.getParameter("lastName").trim();
		String dateOfBirth = request.getParameter("dateOfBirth").trim();
		String flightName = request.getParameter("flightName").trim();
		String departureDate = request.getParameter("departureDate").trim();
		int sizeOfParty = Integer.parseInt(request.getParameter("sizeOfParty").trim());
		Date taxiDate = sdf.parse(request.getParameter("taxiDate").trim());
		String roomType = request.getParameter("roomType").trim();
		String checkInDate = request.getParameter("checkInDate").trim();
		String checkOutDate = request.getParameter("checkOutDate").trim();
		
		// Begin transaction
		utx.begin();
		
		try
		{
			newFlightBooking = fbm.makeBooking(firstName + " " + lastName, flightName, departureDate);
			newTaxiBooking = tbm.book(firstName, lastName, sizeOfParty, taxiDate);
			newHotelBooking = hbm.book(firstName, lastName, dateOfBirth, roomType, checkInDate, checkOutDate);
									
			if(newFlightBooking == null || newTaxiBooking == null || newHotelBooking == null)
				throw new RuntimeException();
		}
		catch(RuntimeException e)
		{
			// Rollback transaction in case of a caught exception
			utx.setRollbackOnly();
			%><strong>Transactions rolled back!</strong><br/><%
		}
		finally
		{
			// Commit transaction if no exceptions caught
			utx.commit();
			%><strong>Transactions successful!</strong><br/><%
		}
	}
	else
	{
		response.sendRedirect("transactions-error.jsp");
	}
%>

<a href="transactions.jsp" title="Transactions">Back to Transactions</a>

</div>
</body>
</html>