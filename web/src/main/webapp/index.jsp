<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Book a hotel</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.min.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>

<div>

	<h1>Hotel booking service</h1>

	<ul>
		<li><a href="simple.jsp" title="Simple Booking">Simple Booking (only hotel service)</a></li>
		<li><a href="travel-agent.jsp" title="Travel Agent">Travel Agent</a></li>
		<li><a href="transactions.jsp" title="Transactions">Travel Agent (Transactions)</a></li>
	</ul>

</div>

</body>
</html>
