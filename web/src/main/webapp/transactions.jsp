<%@ page import="services.beans.HotelBookingManager" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Travel Agent</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.min.css" media="all" />
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
</head>
<body>
<%
	final Hashtable jndiProperties = new Hashtable();
	jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	final Context context = new InitialContext(jndiProperties);
	
	// Inject hotel booking manager bean
	HotelBookingManager hbm = (HotelBookingManager) context.lookup("ejb:/service/HotelBookingManagerBean!services.beans.HotelBookingManager");
	
	List<String> rooms = hbm.getAllRoomTypes();
%>

<form action="transactions-booking.jsp" method="post">

<div class="booking-service">
	
	<h1>Transactions</h1>
	
	<div class="customer-details">
		<h2>Customer details</h2>
		<fieldset>
			<label for="firstName">First name:</label>
			<input type="text" name="firstName" id="firstName" />
		</fieldset>
		<fieldset>
			<label for="lastName">Last name:</label>
			<input type="text" name="lastName" id="lastName" />
		</fieldset>
		<fieldset>
			<label for="dateOfBirth">Date of birth:</label>
			<input type="date" name="dateOfBirth"  id="dateOfBirth" class="datepicker-v1" autocomplete="off" />
		</fieldset>
	</div>
	
	<div class="flight-details">
		<h2>Flight details</h2>
		<fieldset>
			<label for="flightName">Flight name:</label>
			<input type="text" name="flightName" id="flightName" placeholder="SU169" />
		</fieldset>
		<fieldset>
			<label for="departureDate">Departure date:</label>
			<input type="date" name="departureDate" id="departureDate" class="datepicker-v3" />
		</fieldset>
	</div>
	
	<div class="taxi-details">
		<h2>Taxi details</h2>
		<fieldset>
			<label for="sizeOfParty">Size of party:</label>
			<input type="text" name="sizeOfParty" id="sizeOfParty" />
		</fieldset>
		<fieldset>
			<label for="taxiDate">Date:</label>
			<input type="date" name="taxiDate" id="taxiDate" class="datepicker-v3" autocomplete="off" />
		</fieldset>
	</div>
	
	<div class="hotel-details">
		<h2>Hotel details</h2>
		<fieldset>
			<label for="roomType">Room type: </label>
			<select name="roomType" id="roomType">
				<option value="">-- Select type --</option>
				<%
					for(String r : rooms) {
						out.println("<option value=\"" + r + "\">" + r.substring(0,1).toUpperCase() + r.substring(1).toLowerCase() + "</option>");
					}
				%>
			</select>
		</fieldset>
		<fieldset>
			<label for="checkInDate">Check in date:</label>
			<input type="date" name="checkInDate" id="checkInDate" class="datepicker-v2" autocomplete="off" />
		</fieldset>
		<fieldset>
			<label for="checkOutDate">Check out date:</label>
			<input type="date" name="checkOutDate" id="checkOutDate" class="datepicker-v2" autocomplete="off" />
		</fieldset>
	</div>
	
	<div class="confirm">
		<fieldset>
			<input type="hidden" name="formAction" value="TransactionsBooking" />
			<input type="submit" name="submitBooking" id="submitBooking" />
		</fieldset>
	</div>
	
</div>

</form>

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>