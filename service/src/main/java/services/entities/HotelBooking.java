package services.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Booking entity
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class HotelBooking implements Serializable {

	private static final long serialVersionUID = 5367851234840900634L;
	private Long id;
	private Long referenceNumber;
	private HotelCustomer customer;
	private HotelRoom room;
	private String checkInDate;
	private String checkOutDate;
	
	/**
	 * Default constructor
	 */
	public HotelBooking() {
		
	}
	
	/**
	 * @param customer HotelCustomer the customer
	 * @param room HotelRoom the room
	 * @param referenceNumber Long the reference number of the booking
	 * @param checkInDate String the check-in date
	 * @param checkOutDate String the check-out date
	 */
	public HotelBooking(HotelCustomer customer, HotelRoom room, Long referenceNumber, String checkInDate, String checkOutDate)
	{
		this.customer = customer;
		this.room = room;
		this.referenceNumber = referenceNumber;
		this.checkInDate = checkInDate;
		this.checkOutDate = checkOutDate;
	}
	
	/**
	 * @return booking's id in MySQL table
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId()
	{
		return id;
	}
	
	/**
	 * @param id Long the booking's id in the MySQL table
	 */
	public void setId(Long id)
	{
		this.id = id;
	}
	
	/**
	 * @return booking's reference number
	 */
	public Long getReferenceNumber()
	{
		return referenceNumber;
	}
	
	/**
	 * @param referenceNumber Long the booking's reference number
	 */
	public void setReferenceNumber(Long referenceNumber)
	{
		this.referenceNumber = referenceNumber;
	}
	
	/**
	 * @return the customer object
	 */
	@OneToOne(cascade=CascadeType.REMOVE)
	public HotelCustomer getCustomer()
	{
		return customer;
	}
	
	/**
	 * @param customer HotelCustomer the customer
	 */
	public void setCustomer(HotelCustomer customer)
	{
		this.customer = customer;
	}
	
	/**
	 * @return the room object
	 */
	@ManyToOne
	public HotelRoom getRoom()
	{
		return room;
	}
	
	/**
	 * @param room HotelRoom the room
	 */
	public void setRoom(HotelRoom room)
	{
		this.room = room;
	}
	
	/**
	 * @return check-in date
	 */
	public String getCheckInDate()
	{
		return checkInDate;
	}
	
	/**
	 * @param checkInDate String the check-in date
	 */
	public void setCheckInDate(String checkInDate)
	{
		this.checkInDate = checkInDate;
	}
	
	/**
	 * @return check-out date
	 */
	public String getCheckOutDate()
	{
		return checkOutDate;
	}
	
	/**
	 * @param checkOutDate String the check-out date
	 */
	public void setCheckOutDate(String checkOutDate)
	{
		this.checkOutDate = checkOutDate;
	}

}
