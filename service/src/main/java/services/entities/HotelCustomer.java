package services.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Customer entity.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class HotelCustomer implements Serializable {

	private static final long serialVersionUID = -9125726547181859865L;
	private Long id;
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	
	/**
	 * Default constructor
	 */
	public HotelCustomer() {
		
	}
	
	/**
	 * HotelCustomer constructor
	 * 
	 * @param firstName String the customer's first name
	 * @param lastName String the customer's last name
	 * @param dateOfBirth String the customer's date of birth
	 */
	public HotelCustomer(String firstName, String lastName, String dateOfBirth)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * @return customer's id in MySQL table
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId()
	{
		return id;
	}
	
	/**
	 * @param id Long the customer's id in MySQL table
	 */
	public void setId(Long id)
	{
		this.id = id;
	}
	
	/**
	 * @return customer's first name
	 */
	public String getFirstName()
	{
		return firstName;
	}
	
	/**
	 * @param firstName String the customer's first name
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	/**
	 * @return customer's last name
	 */
	public String getLastName()
	{
		return lastName;
	}
	
	/**
	 * @param lastName String the customer's last name
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	/**
	 * @return customer's date of birth
	 */
	public String getDateOfBirth()
	{
		return dateOfBirth;
	}
	
	/**
	 * @param dateOfBirth String the customer's date of birth
	 */
	public void setDateOfBirth(String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}
	
}
