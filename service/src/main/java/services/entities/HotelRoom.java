package services.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Room entity.
 * 
 * @author Kaspars Zarinovs
 *
 */
/**
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Entity
public class HotelRoom implements Serializable {
	
	private static final long serialVersionUID = -5439555977406357816L;
	private Long id;
	private String roomType;
	private int availableRooms;
	private List<HotelBooking> bookings;
	
	/**
	 * Default constructor
	 */
	public HotelRoom() {
		
	}
	
	/**
	 * HotelRoom constructor
	 * 
	 * @param roomType String the type of the room
	 * @param availableRooms int the number of rooms available at once
	 */
	public HotelRoom(String roomType, int availableRooms)
	{
		this.roomType = roomType;
		this.availableRooms = availableRooms;
	}
	
	/**
	 * @return hotel room's id in MySQL table
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId()
	{
		return id;
	}
	
	/**
	 * @param id Long the hotel room's id in MySQL table
	 */
	public void setId(Long id)
	{
		this.id = id;
	}
	
	/**
	 * @return type of the hotel room
	 */
	public String getRoomType()
	{
		return roomType;
	}
	
	/**
	 * @param roomType String the type of the hotel room
	 */
	public void setRoomType(String roomType)
	{
		this.roomType = roomType;
	}
	
	/**
	 * @return the amount of hotel rooms available at once
	 */
	public int getAvailableRooms()
	{
		return availableRooms;
	}
	
	/**
	 * @param availableRooms int the amount of hotel rooms available at once
	 */
	public void setAvailableRooms(int availableRooms)
	{
		this.availableRooms = availableRooms;
	}

	
	/**
	 * @return list of bookings for the specific room
	 */
	@OneToMany(fetch=FetchType.LAZY)
	public List<HotelBooking> getBookings()
	{
		return bookings;
	}
	
	/**
	 * @param bookings list of bookings for the specific room
	 */
	public void setBookings(List<HotelBooking> bookings)
	{
		this.bookings = bookings;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Room [id: " + id + "; type: " + roomType + "; available: " + availableRooms + "]";
	}
	
}
