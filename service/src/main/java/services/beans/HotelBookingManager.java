package services.beans;

import java.util.List;

import javax.ejb.Remote;
import services.entities.HotelBooking;
import services.entities.HotelRoom;

/**
 * Interface for booking manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Remote
public interface HotelBookingManager
{

	/**
	 * Creates a booking for a hotel room. Returns booking's reference number.
	 * Method returns <code>null</code> if any of the later conditions are not met.
	 * 
	 * Check-in and check-out dates must be today or later.
	 * Check-in date must be before check out date.
	 * Date of birth must be today or earlier.
	 * First name and last name can not be empty strings.
	 * Room type must exist.
	 * 
	 * @param firstName String the customer's first name
	 * @param lastName String the customer's last name
	 * @param dateOfBirth String the customer's date of birth. Format: "dd-MM-yyyy".
	 * @param roomType String the type of hotel room
	 * @param checkInDate String check-in date for hotel room
	 * @param checkOutDate String check-out date for hotel room
	 * @return If successful, Long type reference number, otherwise <code>null</code>
	 */
	Long book(String firstName, String lastName, String dateOfBirth, String roomType, String checkInDate, String checkOutDate);
	
	/**
	 * @param referenceNumber Long the booking's reference number
	 * @return If successful <code>true</code>, otherwise <code>false</code>
	 */
	boolean cancel(Long referenceNumber);
	
	/**
	 * Gets the booking object by reference.
	 * 
	 * @param referenceNumber Long the reference number
	 * @return HotelBooking the hotel booking object.
	 * If does not succeed, returns <code>null</code>
	 */
	HotelBooking getBookingByReference(Long referenceNumber);
	
	/**
	 * @return list of all existing room types as a list of Strings.
	 * @see services.beans.HotelRoomManager#getAllRoomTypes()
	 */
	List<String> getAllRoomTypes();
		
}
