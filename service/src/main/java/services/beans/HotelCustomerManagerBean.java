package services.beans;

import javax.ejb.*;
import services.entities.HotelCustomer;

/**
 * Customer manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HotelCustomerManagerBean implements HotelCustomerManager
{
	
	/**
	 * @see services.beans.HotelCustomerManager#createCustomer(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public HotelCustomer createCustomer(String firstName, String lastName, String dateOfBirth)
	{
		HotelCustomer c = new HotelCustomer(firstName, lastName, dateOfBirth);
		return c;
	}

}
