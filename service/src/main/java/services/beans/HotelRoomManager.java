package services.beans;

import java.util.List;

import javax.ejb.Remote;
import services.entities.HotelRoom;

/**
 * Interface for room manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Remote
public interface HotelRoomManager {

	/**
	 * Creates a new room type.
	 * Returns HotelRoom object.
	 * 
	 * @param roomType String the type of room
	 * @param availableRooms int the number of rooms available at once
	 * @return HotelRoom object
	 */
	HotelRoom createRoom(String roomType, int availableRooms);

	/**
	 * Gets room types from all the rows in HotelRoom MySQL table.
	 * Returns <code>null</code> if table is empty.
	 * 
	 * @return List of strings with room types if HotelRoom table is not empty,
	 * otherwise returns <code>null</code>
	 */
	List<String> getAllRoomTypes();
	
	/**
	 * Returns a room object with type <code>roomType</code> from MySQL table HotelRoom
	 * if exists, otherwise returns <code>null</code>
	 * 
	 * @param roomType String type of hotel room.
	 * @return HotelRoom object if exists in HotelRoom MySQL table, otherwise
	 * returns <code>null</code>
	 */
	HotelRoom getRoomByType(String roomType);
	
}
