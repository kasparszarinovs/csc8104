package services.beans;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.*;

import services.beans.HotelRoomManager;
import services.entities.HotelRoom;

/**
 * Room manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class HotelRoomManagerBean implements HotelRoomManager {

	/**
	 * Define entity manager & persistence context.
	 */
	@PersistenceContext(unitName="example")
	private EntityManager em;
	
	/**
	 * @see services.beans.HotelRoomManager#createRoom(java.lang.String, int)
	 */
	@Override
	public HotelRoom createRoom(String roomType, int availableRooms)
	{
		HotelRoom r = new HotelRoom(roomType, availableRooms);
		em.persist(r);
		return r;
	}
	
	/**
	 * @see services.beans.HotelRoomManager#getAllRoomTypes()
	 */
	@Override
	public List<String> getAllRoomTypes()
	{	
		// getResultList() does not throw exceptions if no results are found
		// an empty list is enough in this case
		final String jpaQlQuery = "SELECT roomType FROM " + HotelRoom.class.getSimpleName();
		Query query = em.createQuery(jpaQlQuery);
		return query.getResultList();
	}
	
	/**
	 * @see services.beans.HotelRoomManager#getRoomByType(java.lang.String)
	 */
	@Override
	public HotelRoom getRoomByType(String roomType)
	{
		try
		{
			final String jpaQlQuery = "FROM " + HotelRoom.class.getSimpleName() + " r WHERE r.roomType = :roomType";
			Query query = em.createQuery(jpaQlQuery);
			query.setParameter("roomType", roomType);
			return (HotelRoom) query.getSingleResult();
		} 
		catch(NonUniqueResultException e) { return null; }
		catch(NoResultException e) { return null; }
	}
		
}
