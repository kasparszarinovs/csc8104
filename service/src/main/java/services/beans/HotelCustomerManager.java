package services.beans;

import javax.ejb.Remote;
import services.entities.HotelCustomer;

/**
 * Interface for customer manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@Remote
public interface HotelCustomerManager
{
	
	/**
	 * Creates a new customer. Returns HotelCustomer object.
	 * 
	 * @param firstName String the customer's first name
	 * @param lastName String the customer's last name
	 * @param dateOfBirth String the customer's date of birth
	 * @return HotelCustomer object
	 */
	HotelCustomer createCustomer(String firstName, String lastName, String dateOfBirth);
	
}
