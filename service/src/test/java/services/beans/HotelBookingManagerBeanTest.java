package services.beans;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.core.api.annotation.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import services.entities.HotelBooking;

import javax.ejb.EJB;
import java.io.File;

/**
 * Tests for hotel booking manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@RunWith(Arquillian.class)
public class HotelBookingManagerBeanTest {

    /**
     * Inject hotel booking manager bean.
     */
    @EJB
    private HotelBookingManager hbm;

     /**
     * Create the deployment archive to be deployed by Arquillian.
     *
     * @return a JavaArchive representing the required deployment
     */
    @Deployment
    public static JavaArchive createTestArchive() {

        return ShrinkWrap.create(JavaArchive.class, "test.jar")
                .addPackages(true, "services")
                .addAsManifestResource(new File("./src/main/resources/META-INF/persistence.xml"));
    }
    
    /**
     * Test booking methods.
     * 
     * @throws Exception
     */
    @Test
    public void testBooking() throws Exception
    {
    	Long referenceNumber = hbm.book("john", "doe", "21-09-1987", "single", "28-12-2012", "30-12-2012");
        HotelBooking booking = hbm.getBookingByReference(referenceNumber);
        Assert.assertTrue("Expected first name to be 'john', but it wasn't", booking.getCustomer().getFirstName().equals("john"));
        Assert.assertTrue("Expected last name to be 'doe', but it wasn't", booking.getCustomer().getLastName().equals("doe"));
        Assert.assertTrue("Expected date of birth to be '21-09-1987', but it wasn't", booking.getCustomer().getDateOfBirth().equals("21-09-1987"));
        Assert.assertTrue("Expected room type to be 'single', but it wasn't", booking.getRoom().getRoomType().equals("single"));
        Assert.assertTrue("Expected check-in date to be '28-12-2012', but it wasn't", booking.getCheckInDate().equals("28-12-2012"));
        Assert.assertTrue("Expected check-out date to be '30-12-2012', but it wasn't", booking.getCheckOutDate().equals("30-12-2012"));
    }


}
