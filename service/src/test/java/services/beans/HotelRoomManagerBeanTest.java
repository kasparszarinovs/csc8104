package services.beans;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.core.api.annotation.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import services.entities.HotelRoom;

import javax.ejb.EJB;
import java.io.File;

/**
 *  Arquilian tests for hotel room manager bean.
 * 
 * @author Kaspars Zarinovs &lt;k.zarinovs@ncl.ac.uk&gt;
 *
 */
@RunWith(Arquillian.class)
public class HotelRoomManagerBeanTest {

    /**
     * Inject hotel room manager bean.
     */
    @EJB
    private HotelRoomManager hrm;

     /**
     * Create the deployment archive to be deployed by Arquillian.
     *
     * @return a JavaArchive representing the required deployment
     */
    @Deployment
    public static JavaArchive createTestArchive() {

        return ShrinkWrap.create(JavaArchive.class, "test.jar")
                .addPackages(true, "services")
                .addAsManifestResource(new File("./src/main/resources/META-INF/persistence.xml"));
    }
    
    /**
     * Test room methods.
     * 
     * @throws Exception
     */
    @Test
    public void testRoom() throws Exception
    {
    	HotelRoom room = hrm.createRoom("large", 2);
        Assert.assertTrue("Expected room type to be 'large', but it wasn't", room.getRoomType().equals("large"));
        Assert.assertTrue("Expected available rooms to be 2, but it wasn't", room.getAvailableRooms() == 2);
    }

}
